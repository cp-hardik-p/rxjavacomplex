package com.example.rxjavacomplexoperators

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.BiFunction
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.rxkotlin.toObservable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.Exception

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnEx1.setOnClickListener { startStream() }

        btnEx2.setOnClickListener { startStream2() }

        btnEx3.setOnClickListener { startStream3() }

        btnEx4.setOnClickListener { startStream4() }

        btnEx5.setOnClickListener { startStream5() }

        btnEx6.setOnClickListener { startStream6() }

        btnEx7.setOnClickListener { startStream7() }

        btnEx8.setOnClickListener { startStream8() }

        btnEx9.setOnClickListener { startStream9() }

        btnEx10.setOnClickListener { startStream10() }

        btnEx11.setOnClickListener { startStream11() }

        btnEx12.setOnClickListener { startStream12() }
    }

    private fun startStream() {
        val list: List<String> = listOf("1", "2", "3", "4", "5", "6")

        list.toObservable()

            .subscribeBy(
                onNext = { println(it) },
                onError = { it.printStackTrace() },
                onComplete = { println("onComplete") }
            )
    }

    private fun startStream2() {

        Observable.just("Hello", "world", "How", "are", "you?")

            .subscribe(
                { value -> println("Received: $value") },
                { error -> println("Error: $error") },
                { println("Completed") }
            )
    }

    private fun startStream3() {
        Observable.fromArray("Apple", "Orange", "banana")
            .subscribe { println(it) }
    }

    private fun startStream4() {
        Observable.fromIterable(listOf("Titan", "Fastrack", "Sonata"))
            .subscribe(
                { value -> println("Received:$value") },
                { error -> println("Error: $error") },
                { println("Done") }
            )
    }

    private fun startStream5() {

        getObservableFromList(listOf("Summer", "Winter", "Monsoon"))
            .subscribe(
                { value -> println("Received: $value") },
                { error -> println("Error: $error") },
                { println("Done") }
            )
    }

    private fun startStream6() {
        Observable.just("A", "B", "C", "D", "E", "F", "G", "H")
            .buffer(2)
            .subscribe(
                { value -> println("Received: $value") },
                { error -> println("Error: $error") },
                { println("Done") }
            )
    }

    private fun startStream7() {
        val observable = Observable.fromArray(1, 2, 3, 4)
        val transformation = observable.map { e ->
            e * 2
        }

        val filtered = transformation.filter { e ->
            e > 2
        }

            .subscribe(
                { value -> println("Received: $value") },
                { error -> println("Error: $error") },
                { println("Done") }
            )
    }

    private fun startStream8() {
        Observable.range(1, 20)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

            .filter { e ->
                return@filter e % 2 == 0
            }

            .subscribe(
                { value -> println("Received: $value") },
                { error -> println("Error: $error") },
                { println("Done") }

            )

    }

    private fun startStream9() {

        val list: List<String> = listOf("1", "2", "3", "4", "5", "6", "7")

        val numObservable = list.toObservable()

        val charObservable = Observable.just("A", "B", "C", "D", "E", "F", "G", "H")

        val zipper = BiFunction<String, String, String> { t1, t2 -> "$t1-$t2" }

        Observable.zip(
            numObservable, charObservable,
            zipper
        )
            .subscribeOn(Schedulers.io())
                .subscribe {
                    println(it)
                }




    }


}

private fun startStream10() {

}

private fun startStream11() {

}

private fun startStream12() {

}


private fun getObservableFromList(myList: List<String>) =
    Observable.create<String> { emitter ->
        myList.forEach { kind ->
            if (kind == "") {
                emitter.onError(Exception("Nothing to show"))
            }
            emitter.onNext(kind)
        }
        emitter.onComplete()
    }
